import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';

import { HomePage } from '../pages/home/home';
import { ProductsPage } from '../pages/products/products';
import { ShopsPage } from '../pages/shops/shops';
import { TabsPage } from '../pages/tabs/tabs';
import { ShopinfoPage } from '../pages/shopinfo/shopinfo';
import { ProductinfoPage } from '../pages/productinfo/productinfo';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    ProductsPage,
    ShopsPage,
    ShopinfoPage,
    ProductinfoPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    ProductsPage,
    ShopsPage,
    ShopinfoPage,
    ProductinfoPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
