import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'

import { ShopinfoPage } from '../shopinfo/shopinfo'

/**
 * Generated class for the ShopsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shops',
  templateUrl: 'shops.html',
})
export class ShopsPage {

  url:string = 'http://127.0.0.1:8000/api/';
  agents: any;
  agentsCopy: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, public http: Http, private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShopsPage');

    let loading = this.loadingCtrl.create({
      content: 'Please wait...',
    });

    loading.present();

    this.http.get(this.url+'agents')
      .map(res => res.json())
      .subscribe( data => {
        this.agents = Array.from(data);
        this.agentsCopy = Array.from(data);
        loading.dismiss();
      });

  }

  click(id){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...',
    });

    loading.present();

    this.http.get(this.url+'agents/'+id)
    .map(res => res.json())
    .subscribe(data => {
      let shopinfo = data;
      this.navCtrl.push(ShopinfoPage, {shopinfo: shopinfo});
      loading.dismiss();
    });
  }

  setItems() {
    this.agents = this.agentsCopy;
  }


  filterItems(ev: any) {
    this.setItems();
    let val = ev.target.value;

    if (val && val.trim() !== '') {
    this.agents = this.agents.filter((item) => {
      return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    });
  }
  }

  onClear() {
  let loading = this.loadingCtrl.create({
    content: 'Please wait...',
  });

  loading.present();
    this.setItems();
    loading.dismiss();
  }

}
