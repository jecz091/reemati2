import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { ProductsPage } from '../products/products';
import { ShopsPage } from '../shops/shops';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ProductsPage;
  tab3Root = ShopsPage;

  constructor() {

  }
}
