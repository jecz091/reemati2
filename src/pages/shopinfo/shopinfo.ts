import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'

/**
 * Generated class for the ShopinfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shopinfo',
  templateUrl: 'shopinfo.html',
})
export class ShopinfoPage {

  shopinfo:any;

  url:string = 'http://127.0.0.1:8000/api/';
  products:any;
  shop:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShopinfoPage');

    this.shop = this.navParams.get('shopinfo')
    console.log(this.shopinfo);

    this.http.get(this.url+'agents/'+this.shop.id+'/products')
    .map(res => res.json())
    .subscribe(data => {
        this.products = data
    });

  }




}
