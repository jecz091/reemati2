import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/concatAll'
import { ProductinfoPage } from '../productinfo/productinfo';

/**
 * Generated class for the ProductsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})
export class ProductsPage {

  name:string = 'All';
  url:string = 'http://127.0.0.1:8000/api/';
  products: any = 0;
  category: any;
  all_products: any;
  productsCopy: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, public http: Http, private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductsPage');

    let loading = this.loadingCtrl.create({
      content: 'Please wait...',
    });

    loading.present();

    this.all_products = this.http.get(this.url+'products', {})
    .map(res => res.json());

    this.all_products.subscribe( data => {
      this.products = Array.from(data);
      this.productsCopy = Array.from(data);
      loading.dismiss();
    });


  }

  categories() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Select Categories',
      buttons: [
        {
          text: 'All',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Please wait...',
            });

            loading.present();

              this.name = 'All';
              this.all_products.subscribe( data => {
              this.products = data;
              loading.dismiss();
              });
          }
        },{
          text: 'Appliances',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Please wait...',
            });

            loading.present();

              this.name = 'Appliances';
              this.http.get(this.url+'category/2', {})
              .map(res => res.json())
              .subscribe( data => {
                this.products = data;
                loading.dismiss();
              });

          }
        },{
          text: 'Gadgets',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Please wait...',
            });

            loading.present();

              this.name = 'Gadgets';
              this.http.get(this.url+'category/3', {})
              .map(res => res.json())
              .subscribe( data => {
                this.products = data;
                loading.dismiss();
              });

          }
        },{
          text: 'Jewelry',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Please wait...',
            });

            loading.present();

              this.name = 'Jewelry';
              this.http.get(this.url+'category/1', {})
              .map(res => res.json())
              .subscribe( data => {
                this.products = data;
                loading.dismiss();
              });

          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  product_info(id){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...',
    });

      loading.present();
      this.http.get(this.url+'products/'+id)
      .map(res => res.json())
      .subscribe(data => {
        let productinfo = data;
        this.navCtrl.push(ProductinfoPage, {productinfo: productinfo});
        loading.dismiss();
      });
  }

  setItems() {
    this.products = this.productsCopy;
  }


  filterItems(ev: any) {
      this.setItems();
      let val = ev.target.value;

     if (val && val.trim() !== '') {
      this.products = this.products.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }

  onClear() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...',
    });

    loading.present();
      this.setItems();
      loading.dismiss();
  }

}
