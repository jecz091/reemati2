import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'

/**
 * Generated class for the ProductinfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-productinfo',
  templateUrl: 'productinfo.html',
})
export class ProductinfoPage {

  url:string = 'http://127.0.0.1:8000/api/';
  product:any;
  shop:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductinfoPage');

    this.product = this.navParams.get('productinfo');

    console.log(this.product);

    this.http.get(this.url+'agents/'+this.product.pawn_agent_id)
    .map(res => res.json())
    .subscribe(data => {
        this.shop = data
    });

  }

}
