import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductinfoPage } from './productinfo';

@NgModule({
  declarations: [
    ProductinfoPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductinfoPage),
  ],
})
export class ProductinfoPageModule {}
