import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  url:string = "http://127.0.0.1:8000/api/";
  featured_product:any;
  featured_shop:any;

  constructor(public navCtrl: NavController, public http: Http, private loadingCtrl: LoadingController) {

  }

  ionViewDidLoad() {

    let loading = this.loadingCtrl.create({
        content: 'Please wait...',
    });

    loading.present();

      this.http.get(this.url+'featured_product')
      .map(res => res.json())
      .subscribe( data => {
        this.featured_product = data;
        loading.dismiss();
      });

    this.http.get(this.url+'featured_shop')
      .map(res => res.json())
      .subscribe( data => {
        this.featured_shop = data;
      });



  }

}
